# Pyspark Presentation

Presentation in 2018 to Data Science Nashville on pyspark in Google Cloud.
The code works through the spark as an analytics tool, much like using Pandas.

Also in the presentation is instructions to set up the cluster in Google Cloud.

## Viewing of Documents

The presentation can be viewed in the browser with the document: [SparkPresentation.html](./SparkPresentation.html)

## Background

Donor Bureau asked me to run a project in Google Cloud using pyspark to run a logistic regression.
The regression predicted flights delayed greater than 15 minutes.

## Result

After working through the hurdles of setting up a cluster and have Spark running, writing the code for training
and testing the logistic regression model is straightforward.
The beauty of Spark is the ability to simply add workers as the data set grows.

As for the intent of the project, Donor Bureau found value in Spark on Google Cloud;
however, the solution offered by DataRobot was a better fit for Donor Bureau.
